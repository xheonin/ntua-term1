#include <iostream>

#define INT_MAX 2147483647
#define INT_MIN -2147483648

class bstree {
public:
  bstree() {
    root = nullptr;
    tree_max = INT_MIN;
    tree_min = INT_MAX;
  };

  int height() { return this->node_height(root); }

  void insert(int x) {
    if (root == nullptr) {
      node *p = new node;
      p->info = x;
      p->left = p->right = nullptr;
      root = p;
    } else {
      root = this->node_insert(root, x);
    }

    if (x > tree_max)
      tree_max = x;
    if (x < tree_min)
      tree_min = x;
  }

  int search(int x) {
    int level = 0;
    node *search = node_search(level, root, x);
    if (search == nullptr)
      return 0;
    else
      return level;
  }

  int min() { return tree_min; }

  int max() { return tree_max; }

  void inorder() {
    node_inorder(root);
    printf("end\n");
  }

  void preorder() {
    node_preorder(root);
    printf("end\n");
  }

  void postorder() {
    node_postorder(root);
    printf("end\n");
  }

private:
  struct node {
    int info;
    node *left, *right;
  };
  node *root;

  int tree_min, tree_max;

  int node_height(node *n) {
    if (n == nullptr)
      return 0;
    return 1 +
           std::max(this->node_height(n->left), this->node_height(n->right));
  }

  node *node_insert(node *n, int key) {
    if (n == nullptr) {
      node *p = new node;
      p->info = key;
      p->left = p->right = nullptr;
      return p;
    }
    if (n->info > key) {
      n->left = node_insert(n->left, key);
    } else if (n->info < key) {
      n->right = node_insert(n->right, key);
    }
    return n;
  }

  node *node_search(int &level, node *n, int key) {
    if (n == nullptr)
      return nullptr;
    level++;
    if (n->info == key)
      return n;
    if (n->info > key)
      return node_search(level, n->left, key);
    else
      return node_search(level, n->right, key);
  }

  void node_inorder(node *n) {
    if (n != nullptr) {
      node_inorder(n->left);
      printf("%d ", n->info);
      node_inorder(n->right);
    }
  }

  void node_preorder(node *n) {
    if (n != nullptr) {
      printf("%d ", n->info);
      node_preorder(n->left);
      node_preorder(n->right);
    }
  }

  void node_postorder(node *n) {
    if (n != nullptr) {
      node_postorder(n->left);
      node_postorder(n->right);
      printf("%d ", n->info);
    }
  }
};

int main() {
  int n1, n2, inpt;
  bstree tree;

  scanf("%d", &n1);
  for (int i = 0; i < n1; i++) {
    scanf("%d", &inpt);
    tree.insert(inpt);
  }

  printf("%d\n", tree.height());
  printf("%d %d\n", tree.min(), tree.max());

  scanf("%d", &n2);
  for (int i = 0; i < n2; i++) {
    scanf("%d", &inpt);
    printf("%d ", tree.search(inpt));
  }
  printf("\n");

  tree.inorder();
  tree.preorder();
  tree.postorder();

  return 0;
}
