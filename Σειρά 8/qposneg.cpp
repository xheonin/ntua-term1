#include <iostream>
using namespace std;

class queue {
public:
  queue();
  bool empty();
  void enqueue(int x);
  int dequeue();
  int peek();
  int count;

private:
  struct node {
    int info;
    node *next;
  };
  node *front, *rear;
};

queue::queue() {
  front = rear = NULL;
  count = 0;
}

bool queue::empty() { return front == NULL; }

void queue::enqueue(int x) {
  node *p = new node;
  p->info = x;
  p->next = NULL;
  if (front == NULL)
    front = p;
  else
    rear->next = p;
  rear = p;
  count++;
}

int queue::dequeue() {
  node *p = front;
  int result = front->info;
  if (front == rear)
    rear = NULL;
  front = front->next;
  delete p;
  count--;
  return result;
}

int queue::peek() { return front->info; }

int main() {
  int n;
  queue neg, pos;
  while (scanf("%d", &n) == 1) {
    if (n > 0)
      pos.enqueue(n);
    else if (n < 0)
      neg.enqueue(-n);
  }

  if (neg.count != pos.count) {
    cout << "no" << endl;
    return 0;
  }

  while (pos.empty() == false) {
    if (pos.dequeue() != neg.dequeue()) {
      cout << "no" << endl;
      return 0;
    }
  }

  cout << "yes" << endl;

  return 0;
}
