#include <iostream>
using namespace std;

class list {
public:
  list();
  bool empty();
  int size();
  void add(int k, int x);
  int get(int k);
  void remove(int k);

private:
  struct node {
    int info;
    node *next;
  };
  node *head, *tail;
  int count;
};

list::list() {
  head = tail = nullptr;
  count = 0;
}

bool list::empty() { return head == nullptr; }

int list::size() { return count; }

void list::add(int k, int x) {
    node* new_node = new node;
    new_node->info = x;
    new_node->next = NULL;

    if(k == 1){
        new_node->next = head;
        head = new_node;
    } else {
        node* temp = head;
        while(--k > 1){
            temp = temp->next;
        }
        new_node->next= temp->next;
        temp->next = new_node;
    }
    count++;
}

int list::get(int k) {
    if(k == 1){
        return head->info;
    } else {
        node* temp = head;
        while(--k > 0){
            temp = temp->next;
        }
        return temp->info;
    }
}

void list::remove(int k) {
    if (head == nullptr) return;

    node* temp = head;
    if (k == 1) {
        head = temp->next;
        count--;
        return;
    }

    for (int i = 2; temp != nullptr && i < k; i++) {
        temp = temp->next;
    }
    if (temp == nullptr || temp->next == nullptr) {
        return;
    }
    node *next = temp->next->next;
    temp->next = next;
    count--;
}


int main() {
    list list;

    // Additions
    int n1;
    cin >> n1;
    for (int i = 0; i < n1; i++) {
        int k, x;
        cin >> k >> x;
        list.add(k, x);
    }

    // Removals
    int n2;
    cin >> n2;
    for (int i = 0; i < n2; i++) {
        int k;
        cin >> k;
        list.remove(k);
    }

    // Out
    int n3;
    cin >> n3;
    cout << list.size() << " " << list.get(n3) << endl;

    return 0;
}
