#include <iostream>
#include <string>
#include <vector>
using namespace std;

const int LINE_LEN = 60;

void add_spaces(string &str, int n) {
    for (int i = 0; i < n; i++) {
        str += ' ';
    }
}

int main() {
    vector<string> words;
    string inpt;
    while(cin >> inpt){
        words.push_back(inpt);
    }

    string line = words[0] + ' ';
    int word = 0;
    vector<int> lines;
    for (int i = 1; i < words.size(); i++) {
        line += words[i] + ' ';
        word++;
        if (line.length() > LINE_LEN + 1) {
            line.erase(line.length() - words[i].length() - 1, line.length() - 1);
            lines.push_back(word);
            lines.push_back(LINE_LEN - line.length());
            word = 0;
            line = words[i] + ' ';
        }
    }

    string out;
    int index = 0, count = 0;
    for (int i = 0; i < lines.size(); i += 2) {
        int word_count = lines[i], marigin = lines[i + 1];
        int spaces_default = (word_count + marigin) / (word_count - 1);
        int extra_spaces = (word_count - 1) - (word_count + marigin) % (word_count - 1);
        for (int i = 0; i < word_count; i++) {
            out += words[index];
            index++;
            if (i != word_count - 1) {
                if (count++ < extra_spaces) add_spaces(out, spaces_default);
                else add_spaces(out, spaces_default + 1);
            }
        }
        out += '\n';
        count = 0;
    }
    for (int i = index; i < words.size(); i++) {
        out += words[i];
        if (i != words.size() - 1) out += ' ';
    }
    cout << out << endl;
    return 0;
}
