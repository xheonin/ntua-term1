#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

void read_to_array(int array[], int n) {
    for (int i = 0; i < n; i++) {
        int in;
        if (cin >> in) {
            array[i] = in;
        };
    }
}

vector<int> find_array_differents(int a[], int b[], int n) {
    vector<int> differents = {};
    sort(a, a + n);
    sort(b, b + n);

    for (int i = 0; i < n; i++) {
        if (a[i] != b[i]) {
            differents.push_back(a[i]);
            differents.push_back(b[i]);
        }
    }

    return differents;
}

int main() {
    int n;
    cin >> n;
    int a[n], b[n];
    read_to_array(a, n);
    read_to_array(b, n);

    vector<int> diff = find_array_differents(a, b, n);
    int len = diff.size() / 2;

    if (len == 0) cout << "yes" << endl;
    else {
        sort(diff.begin(), diff.end());
        cout << "no " << diff[0] << " " << diff[len * 2 - 1] << endl;
    }

    return 0;
}
