#include <iostream>

int main() {
    int n, fact = 1, i = 1;
    std::cin >> n;
    do {
        i++;
        fact *= i;
    } while (n >= fact);
    std::cout << i - 1 << std::endl;
    return 0;
}
