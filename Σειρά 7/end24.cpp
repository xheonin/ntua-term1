#include <iostream>

int main() {
  int n;
  std::cin >> n;
  int sq = n * n;
  if (sq % 100 == 24) std::cout << "yes" << std::endl;
  else std::cout << "no" << std::endl;
  return 0;
}
