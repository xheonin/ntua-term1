#include <iostream>

int main() {
  int lmt, count = 0;
  std::cin >> lmt; lmt++;

  if (lmt > 2) count++;
  if (lmt > 3) count++;

  bool sieve[lmt];

  for (int i = 0; i < lmt; i++)
    sieve[i] = false;

  for (int a = 1; a * a < lmt; a++) {
    for (int b = 1; b * b < lmt; b++) {
      int n = (4 * a* a) + (b * b);
      if (n <= lmt && (n % 12 == 1 || n % 12 == 5))
        sieve[n] ^= true;
      n = (3 * a * a) + (b * b);
      if (n <= lmt && n % 12 == 7)
        sieve[n] ^= true;
      n = (3 * a * a) - (b * b);
      if (a > b && n <= lmt && n % 12 == 11)
        sieve[n] ^= true;
    }
  }

  for (int r = 5; r * r < lmt; r++) {
    if (sieve[r]) {
      for (int i = r * r; i < lmt; i += r * r)
        sieve[i] = false;
    }
  }

  for (int x = 5; x < lmt; x++)
    if (sieve[x]) count++;

  std::cout << count << std::endl;
  return 0;
}
