#include <iostream>

int main() {
  int n;
  std::cin >> n;
  if (n < 1000 && n > 99) std::cout << "yes" << std::endl;
  else std::cout << "no" << std::endl;
  return 0;
}
