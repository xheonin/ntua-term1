#include <iostream>

int main() {
  int n;
  std::cin >> n;
  if (n % 7 == 0 && n % 6 != 0) std::cout << "yes" << std::endl;
  else std::cout << "no" << std::endl;
  return 0;
}
