#include <iostream>
using namespace std;

int find_gcd(int a, int b) {
    if (b == 0) return a;
    return find_gcd(b, a % b);
}

int main() {
    int n;
    cin >> n;

    for (int z=0; z<n; z++) {
        char math;
		bool negative = false;
        int a1, p1, a2, p2, a, p;

        cin >> math >> a1 >> p1 >> a2 >> p2;

        if ((p1 == 0 || p2 == 0) || (math == '/' && a2 ==0)) {
            cout << "error" << endl;
            continue;
        }

        switch (math) {
            case '+': {
                a = (a1 * p2) + (a2 * p1);
                p = p1 * p2;
                break;
            }
            case '-': {
                a = (a1 * p2) - (a2 * p1);
                p = p1 * p2;
                break;
            }
            case '*': {
                a = a1 * a2;
                p = p1 * p2;
                break;
            }
            case '/': {
                a = a1 * p2;
                p = p1 * a2;
                break;
            }
        }

		if ((a <= 0 && p <= 0) || (a >= 0 && p >= 0)) negative = false;
		else negative = true;

        int gcd = find_gcd(a, p);
        a /= gcd;
        p /= gcd;

        int i = abs(a) / abs(p);
        a = abs(a) - abs(p) * i;
		
		if (negative) cout << '-' << i << " " << a << " " << abs(p) << endl;
		else cout << i << " " << a << " " << abs(p) << endl;
    }
    return 0;
}
