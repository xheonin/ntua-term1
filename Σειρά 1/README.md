# Εκφωνήσεις Ασκήσεων 1ης Σειράς

## hello1

### Εκφώνηση

Γράψτε ένα πρόγραμμα που να τυπώνει το παρακάτω μήνυμα.

Κάνετε μία υποβολή για κάθε ένα από τα προγράμματα hello1a έως hello1d, που θα βρείτε στην πρώτη σειρά ασκήσεων.

### Περιορισμοί

- **Όριο χρόνου εκτέλεσης**: 1 sec.

### Παράδειγμα εξόδου

```
hello world
```

## hello2

### Εκφώνηση

Γράψτε ένα πρόγραμμα που να τυπώνει τέσσερις φορές το μήνυμα "hello world", σε τέσσερις γραμμές.

Μπορείτε να υποβάλλετε το πρόγραμμα hello2 που θα βρείτε στην πρώτη σειρά ασκήσεων.

### Περιορισμοί

- **Όριο χρόνου εκτέλεσης**: 1 sec.

### Παράδειγμα εξόδου

```
hello world
hello world
hello world
hello world
```

## hello3

### Εκφώνηση

Γράψτε ένα πρόγραμμα που να τυπώνει είκοσι φορές το μήνυμα "hello world", σε είκοσι γραμμές.

Μπορείτε να υποβάλλετε το πρόγραμμα hello3 που θα βρείτε στην πρώτη σειρά ασκήσεων.

### Περιορισμοί

- **Όριο χρόνου εκτέλεσης**: 1 sec.

### Παράδειγμα εξόδου

```
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
hello world
```

## hello4

### Εκφώνηση

Γράψτε ένα πρόγραμμα που να τυπώνει είκοσι φορές το μήνυμα "hello world", σε είκοσι γραμμές. Σε κάθε γραμμή πρέπει να προηγείται ένας αύξων αριθμός. Μπορείτε να υποβάλλετε το πρόγραμμα hello4 που θα βρείτε στην πρώτη σειρά ασκήσεων.

### Περιορισμοί

- **Όριο χρόνου εκτέλεσης**: 1 sec.

### Παράδειγμα εξόδου

```
1 hello world
2 hello world
3 hello world
4 hello world
5 hello world
6 hello world
7 hello world
8 hello world
9 hello world
10 hello world
11 hello world
12 hello world
13 hello world
14 hello world
15 hello world
16 hello world
17 hello world
18 hello world
19 hello world
20 hello world
```

# hello5

### Εκφώνηση

Γράψτε ένα πρόγραμμα που να διαβάζει έναν ακέραιο αριθμό N και να τυπώνει Ν φορές το μήνυμα "hello world", σε Ν γραμμές. Αν η τιμή του N δεν είναι θετική, το πρόγραμμά σας θα πρέπει να τυπώνει το μήνυμα "nothing today".

Μπορείτε να τροποποιήσετε λίγο το πρόγραμμα hello6 που θα βρείτε στην πρώτη σειρά ασκήσεων, ώστε να μην εμφανίζει το εισαγωγικό μήνυμα στο χρήστη, να τυπώνει τα σωστά μηνύματα κάθε φορά, και να κάνει το σωστό έλεγχο σε περίπτωση μη θετικού N.

### Περιορισμοί

- **Όριο χρόνου εκτέλεσης**: 1 sec.

### Παράδειγμα εισόδου

```
3
```

### Παράδειγμα εξόδου

```
hello world
hello world
hello world
```

### Παράδειγμα εισόδου 2

```
7
```

### Παράδειγμα εξόδου 2

```
hello world
hello world
hello world
hello world
hello world
hello world
hello world
```

### Παράδειγμα εισόδου 3

```
-4
```

### Παράδειγμα εξόδου 3

```
nothing today
```
