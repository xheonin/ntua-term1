#include "pzhelp"

int i;
const int n = 20;

PROC hello() {
    WRITELN(i, "hello world");
}

PROGRAM {
    FOR(i, 1 TO n) { hello(); };
}
