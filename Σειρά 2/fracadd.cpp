#include "pzhelp"

PROGRAM {
    int a, b, c, d;

    // First fraction
    a = READ_INT();
    b = READ_INT();

    SKIP_LINE();

    // Second fraction
    c = READ_INT();
    d = READ_INT();

    if (!b == 0 || !d == 0) {
        int mkd = b*d;
        int a_new = (mkd / b) * a;
        int c_new = (mkd / d) * c;
        WRITELN(a_new + c_new, mkd);
    } else {
        WRITELN();
    };
}
