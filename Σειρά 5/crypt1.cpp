#include <iostream>
#include <string>
using namespace std;

int find_index(char list[26], char item) {
    for (int i = 0; i < 26; i++)
        if (list[i] == item) return i;
    return -1;
}

int main() {
    char ab[26], orig[26] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't' ,'u', 'v', 'w', 'x', 'y', 'z'};
    char orig_up[26] = {'A', 'B' ,'C' ,'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    bool check[26];

    for (int i = 0; i < 26; i++) {
        check[i] = false;
        cin >> ab[i];
    }
    for (int i = 0; i < 26; i++) {
        for (int k = 0; k < 26; k++) {
            if (ab[i] == orig[k]) check[k] = true;
        }
    }
    for (int i = 0; i < 26; i++) {
        if (check[i] == false) {
            cout << "error" << endl;
            return 0;
        }
    }

    string s, paragraph, encrypted;
    getline(cin, s);
    while (getline(cin, s)) {
        paragraph += s + "\n";
    }

    for (int i = 0; i < paragraph.length(); i++) {
        int index = find_index(orig, paragraph.at(i));
        int index_up = find_index(orig_up, paragraph.at(i));
        if (index != -1) encrypted += ab[index];
        else if (index_up != -1) encrypted += toupper(ab[index_up]);
        else encrypted += paragraph.at(i);
    }
    encrypted.erase(encrypted.length() - 1);
    cout << encrypted << endl;
    return 0;
}