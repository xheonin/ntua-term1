#include <iostream>
#include <string>
using namespace std;

char orig[26] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't' ,'u', 'v', 'w', 'x', 'y', 'z'};
char orig_up[26] = {'A', 'B' ,'C' ,'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

int find_index(char list[26], char item) {
    for (int i = 0; i < 26; i++)
        if (list[i] == item) return i;
    return -1;
}

string encrypt(string original, char pass[26]) {
    string encrypted;
    for (int i = 0; i < original.length(); i++) {
        int index = find_index(orig, original.at(i));
        int index_up = find_index(orig_up, original.at(i));
        if (index != -1) encrypted += pass[index];
        else if (index_up != -1) encrypted += toupper(pass[index_up]);
        else encrypted += original.at(i);
    }
    encrypted.erase(encrypted.length() - 1);
    return encrypted;
}

string decrypt(string encrypted, char pass[26]) {
    char pass_up[26];
    for (int i = 0; i < 26; i++) {
        pass_up[i] = toupper(pass[i]);
    }

    string decrypted;
    for (int i = 0; i < encrypted.length(); i++) {
        int index = find_index(pass, encrypted.at(i));
        int index_up = find_index(pass_up, encrypted.at(i));
        if (index != -1) decrypted += orig[index];
        else if (index_up != -1) decrypted += orig_up[index_up];
        else decrypted += encrypted.at(i);
    }
    decrypted.erase(decrypted.length() - 1);
    return decrypted;
}

int main() {
    char pass[26];
    bool check[26];

    for (int i = 0; i < 26; i++) {
        check[i] = false;
        cin >> pass[i];
    }
    for (int i = 0; i < 26; i++) {
        for (int k = 0; k < 26; k++) {
            if (pass[i] == orig[k]) check[k] = true;
        }
    }
    for (int i = 0; i < 26; i++) {
        if (check[i] == false) {
            cout << "error" << endl;
            return 0;
        }
    }

    string cmd;
    getline(cin, cmd);
    getline(cin, cmd);

    string s, paragraph;
    while (getline(cin, s)) {
        paragraph += s + "\n";
    }

    if (cmd.at(0) == 'd') cout << decrypt(paragraph, pass) << endl;
    else cout << encrypt(paragraph, pass) << endl;
    
    return 0;
}