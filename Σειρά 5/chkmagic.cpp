#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;
    int table[n][n];
    bool is_valid = true, values[n * n - 1];
    for (int i = 0; i < n * n - 1; i++) values[i] = false;

    for (int i = 0; i < n; i++) {
        for (int k = 0; k < n; k++) {
            int in;
            if (cin >> in) {
                table[i][k] = in;
                if (in > n*n-1) is_valid = false;
                values[in] = true;
            };
        }
    }
    for (int i = 0; i < n * n - 1; i++) {
        if (values[i] == false) is_valid = false;
    }

    int sum = 0, sum_d1 = 0, sum_d2 = 0;
    for (int i = 0; i < n; i++) {
        sum_d1 += table[i][i];
        sum_d2 += table[n - i - 1][i];

        int sum_h = 0, sum_v = 0;
        for (int k = 0; k < n; k++) {
            sum_h += table[i][k];
            sum_v += table[k][i];
        }
        if (i == 0 && sum_h == sum_v) sum = sum_h;
        if (sum_h != sum || sum_v != sum) is_valid = false;
    }
    if (sum_d1 != sum || sum_d2 != sum) is_valid = false;

    if (!is_valid) cout << "no" << endl;
    else cout << "yes" << endl;
    return 0;
}
