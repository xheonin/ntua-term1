#include <iostream>
using namespace std;

int pow(int x, int p) {
    if (p == 0) return 1;
    if (p == 1) return x;
    int tmp = pow(x, p / 2);
    if (p % 2 == 0) return tmp * tmp;
    else return x * tmp * tmp;
}

int main() {
    int n, powers[10], cache[9999];
    cin >> n;
    cout << 0 << endl;
    for (int i = 0; i < 10; i++) {
        powers[i] = pow(i, n);
    }
    for (int i = 1; i <= 4; i++) {
        int up_lim = pow(10, i), down_lim = pow(10, i - 1);
        for (int j = down_lim; j < up_lim; j++) {
            int sum = 0;
            for (int k = 0; k < i; k++) {
                int a = (j % pow(10, k + 1)) / pow(10, k);
                sum += powers[a];
            }
            cache[j] = sum;
            if (j == sum) cout << j << endl;
        }
    }
    for (int i = 1; i < 10000; i++) {
        for (int k = 1; k < 10000; k++) {
            int num = i * 10000 + k;
            if (cache[i] + cache[k] == num) cout << num << endl;
        }
    }
    return 0;
}
