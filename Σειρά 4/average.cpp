#include <iostream>
#include <bits/stdc++.h>
using namespace std;

int main() {
    int n, m;
    cin >> n >> m;
    double table[n][m];
    for (int i = 0; i < n; i++) {
        for (int k = 0; k < m; k++) {
            double in;
            if (cin >> in) table[i][k] = in;
        }
    }
    double ravg = 0, lavg = 0;
    for (int i = 0; i < n; i++) {
        double tmp = 0;
        for (int k = 0; k < m; k++) {
            tmp += table[i][k];
        }
        lavg += tmp / m;
    }
    for (int k = 0; k < m; k++) {
        double tmp = 0;
        for (int i = 0; i < n; i++) {
            tmp += table[i][k];
        }
        ravg += tmp / n;
    }
    lavg /= n; ravg /= m;
    cout << fixed << setprecision(3) << setfill('0');
    cout << setw(3) << lavg << endl << ravg << endl;
    return 0;
}
