#include <iostream>
#include <bits/stdc++.h>
using namespace std;

int main() {
    int n, m;
    cin >> n >> m;
    int table[n][m];
    for (int i = 0; i < n; i++) {
        for (int k = 0; k < m; k++) {
            int in;
            if (cin >> in) table[i][k] = in;
        }
    }
    int rmin = INT_MAX, lmax = 0;
    for (int k = 0; k < m; k++) {
        int max = 0;
        for (int i = 0; i < n; i++) {
            if (table[i][k] > max) max = table[i][k];
        }
        if (rmin > max) rmin = max;
    }
    for (int i = 0; i < n; i++) {
        int min = INT_MAX;
        for (int k = 0; k < m; k++) {
            if (table[i][k] < min) min = table[i][k];
        }
        if (lmax < min) lmax = min;
    }
    cout << rmin << endl << lmax << endl;
    return 0;
}
