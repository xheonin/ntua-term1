#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

bool is_palin(string str) {
    int len = str.length();
    for (int i = 0; i < len / 2; i++) {
        if (str.at(i) != str.at(len - i - 1)) return false;
    }
    return true;
}

int main() {
    int n, successful = 0;
    string tmp;
    cin >> n;
    getline(cin, tmp);

    for (int i = 0; i < n; i++) {
        string str;
        getline(cin, str);
        if (str.length() == 0) {
            cout << "empty" << endl;
            continue;
        }
        else if (str.length() > 20) {
            cout << "error" << endl;
            continue;
        }
        if (is_palin(str)) {
            cout << "yes" << endl;
            successful++;
        } else cout << "no" << endl;
    }
    double perc = ((1.0 * successful) / (1.0 * n)) * 100.0;
    cout << fixed << setprecision(3) << setfill('0');
    cout << setw(3) << perc << endl;
    return 0;
}
